#!/usr/bin/env python

import sys
import os
import time
import textwrap
import warnings
import cPickle as pickle
from collections import OrderedDict

import numpy as np
import numpy.random as rnd
import pandas as pd
import scipy as sp
from scipy.special import gammaln

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from theano import sparse

warnings.filterwarnings('ignore')
import lasagne
warnings.resetwarnings()

from utils import *

msg_init = textwrap.dedent(
    '''
        ARTM-SVI class initialized.
        Topics numer = %d;
        Optimization with %s;
    '''
)

msg_learn = textwrap.dedent(
    '''
        ARTM-SVI start learning.
        Learning rate = %f;
        max_iter = %d;
    '''
)


class ARTM_SVE(object):
    def __init__(self, topics_n, words_n, docs_n,
                 init_reg_vec=None, random_seed=42, use_sparse=False,
                 optimization='adam', learning_rate=1e-2,
                 learn_tau=True, log_dot_exp_option='eps'):

        sgd_update_function = {
            'adam': lasagne.updates.adam,
            'adagrad': lasagne.updates.adagrad,
            'sgd': lasagne.updates.sgd,
            'adadelta': lasagne.updates.adadelta
        }.get(optimization, lasagne.updates.adam)

        self.topics_n = topics_n
        self.docs_n = docs_n
        self.words_n = words_n
        self.use_sparse = use_sparse

        # Declare reg coefficients
        if init_reg_vec is None:
            init_reg_vec = [0.0, 0.0]

        tau_names = ['tau_%d' % i for i, _ in enumerate(init_reg_vec)]
        for i, val in enumerate(init_reg_vec):
            setattr(self, tau_names[i], theano.shared(val))

        self.tau = tuple([getattr(self, tau_i) for tau_i in tau_names])

        # Declare the variable for matrix of counts
        if self.use_sparse:
            x_train = sparse.csr_matrix(name='x_train', dtype='float64')
        else:
            x_train = T.matrix('x_train')

        # Declare phi and theta
        srng = RandomStreams(seed=random_seed)
        
        phi_zeros = np.zeros((self.words_n, self.topics_n))
        theta_zeros = np.zeros((self.topics_n, self.docs_n))

        mu_phi = theano.shared(np.zeros_like(phi_zeros), name='mu_phi')
        log_sigma_phi = theano.shared(np.zeros_like(phi_zeros), name='log_sigma_phi')
        mu_theta = theano.shared(np.zeros_like(theta_zeros), name='mu_theta')
        log_sigma_theta = theano.shared(np.zeros_like(theta_zeros), name='log_sigma_theta')

        sigma_phi = T.exp(log_sigma_phi)
        sigma_theta = T.exp(log_sigma_theta)

        eps_phi = srng.normal(size=(self.words_n, self.topics_n))
        eps_theta = srng.normal(size=(self.topics_n, self.docs_n))

        log_phi = log_softmax(eps_phi * sigma_phi + mu_phi)
        log_theta = log_softmax(eps_theta * sigma_theta + mu_theta)
        self.phi = T.exp(log_phi)
        self.theta = T.exp(log_theta)
        phi_x_theta = log_dot_exp(log_phi, log_theta, mode=log_dot_exp_option)

        # Declaring the elbo

        if self.use_sparse:
            log_likelihood = sparse.basic.sp_sum(phi_x_theta * x_train)
        else:
            log_likelihood = T.sum(phi_x_theta * x_train)

        regularizer_vec = T.sum(log_phi), T.sum(log_theta)
        regularizer = self.tau[0] * regularizer_vec[0] + self.tau[1] * regularizer_vec[1]

        self.elbo_unnormalized = log_likelihood
        self.elbo_unnormalized = self.elbo_unnormalized - T.sum(log_phi) - T.sum(log_theta)\
             - T.sum(log_sigma_phi) - T.sum(log_sigma_theta)  # log q
        self.elbo_unnormalized = self.elbo_unnormalized + regularizer  # log \hat{p}
        self.get_elbo = theano.function([x_train], self.elbo_unnormalized - self.get_dirichilet_logz(), name='get_elbo')

        # Declare the metrics
        perplexity = T.exp(- 1.0 / T.sum(x_train) * log_likelihood)
        self.calculate_perplexity = theano.function([x_train], perplexity)

        theta_nnz_threshold = T.dscalar('theta_threshold')
        phi_nnz_threshold = T.dscalar('phi_threshold')
        nnz_phi = T.sum(self.phi >= phi_nnz_threshold)
        nnz_theta = T.sum(self.theta >= theta_nnz_threshold)
        self.calculate_nnz = theano.function(
            inputs=[phi_nnz_threshold, theta_nnz_threshold],
            outputs=[nnz_phi, nnz_theta]
            )

        self.params = (mu_phi, mu_theta, log_sigma_phi, log_sigma_theta)

        if learn_tau:
            params_to_learn = self.tau + self.params
        else:
            params_to_learn = self.params

        self.learning_rate = theano.shared(1e-2)
        sgd_update = sgd_update_function(-self.elbo_unnormalized, params_to_learn, self.learning_rate)
        self.train_epoch = theano.function(
            inputs=[x_train],
            outputs=[self.elbo_unnormalized, regularizer, self.tau[0], self.tau[1]],
            updates=sgd_update,
            name='train'
            )
        self.metrics_total = dict(epoch=[], elbo=[], perp=[], reg=[], tau_0=[], tau_1=[],
                                  nnz_phi_1=[], nnz_theta_1=[], nnz_phi_2=[], nnz_theta_2=[],
                                  den_phi_1=[], den_theta_1=[], den_phi_2=[], den_theta_2=[])

        print msg_init % (topics_n, optimization)

    def get_dirichilet_reg_values(self):
        """
            For Contrastive Divergence
        :return: regularization coefficients for Phi and Theta sampled for real Dirichilet prior
        """
        alpha_0, alpha_1 = self.tau[0].get_value() + 1, self.tau[1].get_value() + 1
        phi_est = rnd.dirichlet(alpha_0 * np.ones(self.words_n), size=self.topics_n).T
        theta_est = rnd.dirichlet(alpha_1 * np.ones(self.topics_n), size=self.docs_n).T
        return np.sum(np.log(phi_est)) * (alpha_0 - 1), np.sum(np.log(theta_est)) * (alpha_1 - 1)

    def get_dirichilet_logz(self):
        alpha_0, alpha_1 = self.tau[0].get_value() + 1, self.tau[1].get_value() + 1
        n, m = self.words_n, self.topics_n
        ans = m * (n * gammaln(alpha_0) - gammaln(alpha_0 * n))
        n, m = self.topics_n, self.docs_n
        ans += m * (n * gammaln(alpha_1) - gammaln(alpha_1 * n))
        return ans

    def train(self, count_matrix, max_iter=1000, handle_z='Skip', steps_to_print=1, nnz_thresh=1e-2, learning_rate=None,
              learning_rate_decay=False):
        if learning_rate is not None:
            self.learning_rate.set_value(learning_rate)
        print msg_learn % (self.learning_rate.get_value(), max_iter)

        lr_start = self.learning_rate.get_value()
        start_time = time.time()
        print 'Iter\tTime\tELBO value\tPerplexity\ttau_0\ttau_1\tPhi_den\tTheta_den'

        for epoch in xrange(max_iter):
            if learning_rate_decay:
                self.learning_rate.set_value(lr_start / (epoch + 1) ** 0.5)

            cur_elbo = self.get_elbo(count_matrix)
            if np.isinf(cur_elbo) or np.isnan(cur_elbo):
                print 'Learning failed. ELBO=', cur_elbo
                break

            cur_elbo_unnorm, reg, tau_0_val, tau_1_val = self.train_epoch(count_matrix)

            if handle_z == 'MCMC':
                # MCMC update for Dirichilet
                cur_reg_vec = self.get_dirichilet_reg_values()

                self.tau[0].set_value(self.tau[0].get_value() - self.learning_rate * cur_reg_vec[0])
                self.tau[1].set_value(self.tau[1].get_value() - self.learning_rate * cur_reg_vec[1])
            elif handle_z == 'Dir':
                pass
            elif handle_z == 'IS':
                pass
            elif handle_z == 'Skip':
                pass
            else:
                print 'Unknown value %s' % handle_z
                break

            # Calculate Metrics
            perp = self.calculate_perplexity(count_matrix)
            nnz_phi_val_1, nnz_theta_val_1 = self.calculate_nnz(nnz_thresh / self.words_n, nnz_thresh / self.topics_n)
            nnz_phi_val_2, nnz_theta_val_2 = self.calculate_nnz(10 * nnz_thresh / self.words_n, nnz_thresh / self.topics_n)

            result_update(self.metrics_total,
                          epoch=epoch,
                          elbo=cur_elbo,
                          reg=reg,
                          perp=perp,
                          tau_0=tau_0_val,
                          tau_1=tau_1_val,
                          nnz_phi_1=nnz_phi_val_1,
                          nnz_theta_1=nnz_theta_val_1,
                          nnz_phi_2=nnz_phi_val_2,
                          nnz_theta_2=nnz_theta_val_2,
                          den_phi_1=float(nnz_phi_val_1) / self.words_n / self.topics_n,
                          den_theta_1=float(nnz_theta_val_1) / self.topics_n / self.docs_n,
                          den_phi_2=float(nnz_phi_val_2) / self.words_n / self.topics_n,
                          den_theta_2=float(nnz_theta_val_2) / self.topics_n / self.docs_n)

            if epoch % steps_to_print == 0:
                print '%d\t%0.2f\t%e\t%e\t%0.4f\t%0.4f\t%0.4f\t%0.4f' % (
                    epoch, time.time() - start_time, cur_elbo, perp, tau_0_val, tau_1_val,
                    float(nnz_phi_val_2) / self.words_n / self.topics_n,
                    float(nnz_theta_val_2) / self.topics_n / self.docs_n
                )
