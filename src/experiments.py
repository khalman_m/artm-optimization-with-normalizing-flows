#!/bin/env python
import sys

import argparse
import cPickle as pickle
import numpy as np
import pandas as pd

import artm_sve


def get_data(filename):
    print 'Reading the data from %s' % filename
    count_matrix_sp = pickle.load(open(filename))
    print '%d words, %d documents (%0.1fMb)' % \
          (count_matrix_sp.shape[0],
           count_matrix_sp.shape[1],
           count_matrix_sp.data.nbytes * 1.0 / 1024 / 1024)
    count_matrix_den = count_matrix_sp.todense()
    print 'In dense: (%dMb)' % \
        (count_matrix_den.nbytes / 1024 / 1024)
    return count_matrix_den


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--topics-number", type=int,
                        help="number of topics",
                        default=10)
    parser.add_argument("-m", "--max-iter", type=int,
                        help="number of iterations",
                        default=1000)
    parser.add_argument("-s", "--steps-print", type=int,
                        help="number of iteration after that to print the output",
                        default=100)
    parser.add_argument("-l", "--learning-rate", type=float,
                        help="learning rate for the optimization",
                        default=1e-2)
    parser.add_argument("--log-dot-exp", choices=['def', 'eps', 'tensor', 'clip'],
                        help="choose the way to perforom log_dot_exp operation",
                        default='def')
    parser.add_argument("-a", "--adaptive", choices=['adam', 'adagrad', 'sgd', 'adadelta'],
                        help="choose the optimization method",
                        default='adam')
    parser.add_argument("-o", "--output", type=str,
                        help="choose the optimization method",
                        default='dump_results')


    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args(sys.argv)
    count_matrix = get_data('../data/NIPS-collection/nips_train_sp.p')
    n_topics = args.topics_number
    n_words, n_docs = count_matrix.shape

    model = artm_sve.ARTM_SVE(n_topics, n_words, n_docs,
                              learn_tau=False, log_dot_exp_option=args.log_dot_exp,
                              optimization=args.adaptive
                              )
    model.train(count_matrix,
                max_iter=args.max_iter,
                learning_rate=args.learning_rate,
                learning_rate_decay=True,
                steps_to_print=args.steps_print)

    pickle.dump(model.metrics_total, open(args.output, 'wb'))
    print 'Finished. Dict with metrics dumped into %s' % args.output


"""
figure(figsize=(16, 9))

things_to_plot = ['elbo', 'perp', 'tau_0', 'tau_1', 'den_phi', 'den_theta']
titles = ['ELBO', 'Perplexity', 'tau0', 'tau1',  'Phi density', 'Theta_Density']

for i in xrange(6):
    subplot(3, 2, i + 1)
    plot(model.metrics_total[things_to_plot[i]])
    #xlim([2500, 3000])
    title(titles[i])
"""