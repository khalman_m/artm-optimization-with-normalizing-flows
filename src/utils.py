import theano
import theano.tensor as T
import numpy as np
from pylearn2.expr.basic import log_sum_exp


def sum_log_nnz(x):
    return T.sum(T.log(T.nonzero_values(x)))


def mean_log_nnz(x):
    return T.sum(T.log(T.nonzero_values(x))) / x.size


def normalize(x):
    return x / T.sum(x, axis=0)


def log_softmax(x):
    return x - log_sum_exp(x, axis=0)


def log_dot_exp(x, y, mode='def', eps=1e-8):
    """
    x, y -- matricies.
    returns log(exp(x)*exp(y)), where 
        log and exp are elementwise operations
        product '*' is matrix dot-product
    """
    if mode == 'def':
        max_x = T.max(x, axis=1, keepdims=True)
        max_y = T.max(y, axis=0, keepdims=True)
        return T.log(T.dot(T.exp(x - max_x), T.exp(y - max_y))) + max_x + max_y
    elif mode == 'eps':
        max_x = T.max(x, axis=1, keepdims=True)
        max_y = T.max(y, axis=0, keepdims=True)
        return T.log(T.dot(T.exp(x - max_x), T.exp(y - max_y)) + eps) + max_x + max_y
    elif mode == 'clip':
        max_x = T.max(x, axis=1, keepdims=True)
        max_y = T.max(y, axis=0, keepdims=True)
        return T.log(T.clip(T.dot(T.exp(x - max_x), T.exp(y - max_y)), eps, np.inf)) + max_x + max_y
    elif mode == 'tensor':
        x_3d = x.dimshuffle((0, 'x', 1))
        y_3d = y.dimshuffle(('x', 1, 0))
        return log_sum_exp(x_3d + y_3d, axis=2)
    else:
        print 'Undefined mode'
        return


def result_update(result, **kwargs):
        for k in kwargs:
            result[k].append(kwargs[k])

