% !TeX spellcheck = Russian
\documentclass[10pt,a4paper]{article}
%\usepackage[T2A,T1]{fontenc}
%\usepackage[utf8]{inputenc}
%\usepackage[english,russian]{babel}
\usepackage{fontspec}
\usepackage{polyglossia} %% added for proper hyphening
\setmainfont{Georgia}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsbsy}
\usepackage{dsfont}
\usepackage{graphicx}
\usepackage{color}
\usepackage[]{algorithm}
\usepackage[]{algorithmic}
\usepackage[makeroom]{cancel} % cancels (crosses)

% algorithms
\def\algorithmicrequire{\textbf{Data:}}
\def\algorithmicensure{\textbf{Result:}}
% переопределение стиля комментариев
\def\algorithmiccomment#1{\quad// {\sl #1}}

\let\oldref\ref
\newcommand{\KL}[2]{\operatorname{KL}\left(#1 \left\vert\vphantom{#1#2}\right\vert #2\right)}
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\left(#1\right)}}
\renewcommand{\ref}[1]{(\oldref{#1})}
\renewcommand{\Pr}{\mathds{P}}
\newcommand{\R}{\mathbb{R}}
\renewcommand{\vec}{\boldsymbol}

\newcommand{\softmax}{\operatorname{softmax}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\data}{\mathrm{X}}
\newcommand{\mean}{\mathds{E}}
\newcommand{\eps}{\varepsilon}
\renewcommand{\phi}{\varphi}

\title{Оптимизация параметров регуляризации для тематической модели ARTM при помощи stochastic backpropagation и normalizing flows}
\author{Михаил Хальман}

\begin{document}

\maketitle

\section{Абстрактно}

Мы описываем распределение на данные (распределение слов в документах) $\vec{x}$ через латентные переменные $\vec{t}$ (распределения слов в темах и распределения тем в документах). Параметры модели $\vec{\theta}$ задают априорное распределение на $\vec{t}$.

\begin{equation*}
\begin{aligned}
p(\vec{x}, \vec{t} | \vec{\theta}) = p(\vec{x} | \vec{t})p(\vec{t} | \vec{\theta})
\end{aligned}
\end{equation*}
Априорное распределение на $t$ известно с точностью до нормировочной константы: 
\begin{equation*}
p(\vec{t} | \vec{\theta}) \propto \hat{p}(\vec{t} | \vec{\theta}) = \exp(\vec{\theta}^\top \vec{R}(\vec{t}))
\end{equation*}
, где $\vec{R}(\vec{t})$ --- вектор значений регуляризаторов ARTM, $\vec{\theta}$ --- коэффициенты регуляризации.

Задача состоит в том, чтобы оптимизировать коэффициенты регуляризации $\vec{\theta}$ в соответствии с идеей maximum-evidence:

\begin{equation*}
\begin{aligned}
\vec{\theta^{*}} = \argmax_{\vec{\theta}} p(\vec{x} | \vec{\theta}) = \argmax_{\vec{\theta}} \log p(\vec{x} | \vec{\theta})
\end{aligned}
\end{equation*}

Сложность задачи состоит в том, что напрямую оптимизировать $\log p(\vec{x} | \vec{\theta})$ представляется затруднительным. 
\footnote{
Вычисление, как градиентов, так и самой функции сводится к вычислению многомерных интегралов в пространствах большой размерности:

\begin{equation*}
p(\vec{x} | \vec{\theta}) = \int p(\vec{x} | \vec{t}) p(\vec{t} | \vec{\theta}) dt ,
\end{equation*}

 где интеграл берётся по всем допустимым векторам $\vec{t}$, то есть по всем распределениям слов в топиках и топиков в документах. 
}

Основная идея вариационного вывода заключается в том, чтобы вместо оптимизации intractable логарифма правдоподобия $\log p(\vec{x} | \vec{\theta})$, оптимизировать его tractable вариационную нижнюю границу (ELBO) $\mathcal{L}(\vec{\theta}, \vec{\lambda})$ как по параметрам $\vec{\theta}$, так и по вариационным параметрам нижней границы $\vec{\lambda}$.

Для того, чтобы задать нижнюю границу, вводится распределение $q(\vec{t} | \vec\lambda) \equiv q_\lambda(\vec{t}) \in \mathcal{F}$  которое приближает апостериорное распределение $p(\vec{t} | \vec{x})$. Вариационный параметр $\vec\lambda$ параметризует конкретное распределение $q_\lambda$ в заданном семействе $\mathcal{F}$.

\footnote{Шаг оптимизации по $\vec{\lambda}$ будет приближать приближение $q(\vec{t} | \vec\lambda)$ к $p(\vec{t} | \vec{x})$. Шаг оптимизации по $\vec{\theta}$ будет максимизировать наше приближение правдоподобия для новой, уточнённой аппроксимации.}

\begin{equation}
\begin{aligned}
\log p(\vec{x} | \vec{\theta}) 
\geq 
\mathcal{L} (\vec{\theta}, \vec{\lambda}) 
= 
\int 
	q(\vec{t} | \vec{\lambda}) 
	\log 
		\frac
			{p(\vec{x}, \vec{t} | \vec{\theta})} 
			{q(\vec{t} | \vec{\lambda})}
= \\ =
\mean_{q_\lambda}
	\log p(\vec{x} | \vec{t}, \cancel{\vec{\theta}}) 
-
\mean_{q_\lambda}
	\log
	\frac
		{q(\vec{t} | \vec\lambda)}
		{\hat{p}(\vec{t} | \vec\theta)}
-
	\log
		Z(\vec\theta),
\end{aligned}
\label{eq:elbo}
\end{equation}
где $Z(\vec\theta) = \int \hat{p}(\vec{t} | \vec\theta) d\vec{t}$ --- нормировочная константа априорного распределения.


Для оптимизации ELBO \ref{eq:elbo} необходимо вычислять градиенты $\mathcal{L}$ по $\vec\theta$ и $\vec\lambda$. Для эффективного вычисления градиентов воспользуемся методом Монте-Карло и reparametrization trick. Для этого зададим $\mathcal{F}$ как семейство распределений, полученных дифференцируемым по параметру $\lambda$ преобразованием $g(\vec{\eps}, \vec{\lambda})$ распределения из простого семейства $q_0(\vec{\eps})$, допускающего эффективное сэмплирование. (Например MLP от стандартного нормального шума с единичной матрицей ковариации) 

Для таких распределений мы можем воспользоваться следующим преобразованием:

\begin{equation*}
\nabla_\lambda 
	\mean_{ q(\vec{t} | \vec{\lambda}) }
		f(\vec{t})
=
\mean_{q_0(\vec\eps)}
	\nabla_\lambda
		f(g(\vec\eps, \vec\lambda)) 	
\end{equation*}

Тогда частные производные $\frac{ \partial \mathcal{L}} {\partial \vec\lambda}$ и  $\frac{ \partial \mathcal{L}} {\partial \vec\theta}$ примут вид:

\begin{equation}
\begin{aligned}
\frac
	{\partial \mathcal{L}}
	{\partial \vec\lambda}
&=
	\mean_{q_0} 
		\nabla_{\vec\lambda}
			\log 
				p(\vec{x} | g(\vec\eps, \vec\lambda))
	-
	\mean_{q_0} 
		\nabla_{\vec\lambda}
			\log 
				q(g(\vec\eps, \vec\lambda) | \lambda)
	+
	\mean_{q_0} 
		\nabla_{\vec\lambda}
			\log 
				\hat{p}(g('\vec\eps, \vec\lambda) | \theta)
\\
\frac
	{\partial \mathcal{L}}
	{\partial \vec\theta}
&=
	\mean_{q_0} 
		\nabla_{\vec\theta}
			\log 
				\hat{p}(g(\vec\eps, \vec\lambda) | \theta)
	-
\mean_{q_0}
	\nabla_{\vec\theta}
		\log
			Z(\vec\theta)
\end{aligned}
\label{eq:gradients_abstr}
\end{equation}

Шаблон алгоритма обучения выглядит следующим образом:

\begin{algorithm}
	\caption{Алгоритм обучения (скетч)}
	\label{alg1}
	\begin{algorithmic}
		\REQUIRE Flow of data $\vec{x}$; learning rate $\kappa$
		\ENSURE $\vec\lambda$, $\vec\theta$
		\STATE initialize $\vec\lambda$ and $\vec\theta$
		\REPEAT 
		\STATE $\vec{x} \leftarrow $ \{Get mini-batch\}
		\STATE Sample $\vec\eps \leftarrow q_0$ i.i.d componentwise \
		\STATE Compute stochastic estimations of gradients over $\vec\theta$ and $\vec\lambda$ accroding to \ref{eq:gradients_abstr}
		\STATE $\vec\theta = \vec\theta - \kappa \frac{\partial \mathcal{L}}{\partial \vec\theta}$
		\STATE $\vec\lambda = \vec\lambda - \kappa \frac{\partial \mathcal{L}}{\partial \vec\lambda}$ 
		\COMMENT or any other stochastic-gradient-kind optimization algorithm, e.g. AdaGrad
		\UNTIL convergence
	\end{algorithmic}
\end{algorithm}

\section{Конкретно}

Рассмотрим теперь вероятностную тематическую модель ARTM.

Пусть дано множество документов $D$ и словарь $W$. Каждый документ $d$ будем отождествлять с его индексом в коллекции, а каждое слово $w$ с его индексом в словаре. В соответствии с моделью мешка слов, каждый документ будем представлять вектором длины $W$ частот слов в  нём.

Введём обозначения:
\begin{itemize}
	\item $n_{dw}$ --- сколько раз слово $w$ встретилось в документе $d$.
	\item $n_d$ --- число слов в документе $d$.
	\item $N={n_{wd}} \in \mathbb{R}^{W \times D}$  --- матрица частот слов в документах.
	\item $F={f_{wt}}  \in \mathbb{R}^{W \times T}$,  $f_{wd} = n_{dw} / n_d$ --- нормированная матрица частот
\end{itemize}

 В ARTM задача тематического моделирования сводится к поиску приближения матрицы $F$ в виде произведения $F \thickapprox \Phi \Theta$ двух стохастических матриц $\Phi$ и $\Theta$, где $\Phi$ и $\Theta$ находятся как решение задачи оптимизации:
\begin{gather*}
\sum_{d=1}^D n_d \KL{F_d}{\left(\Phi\Theta\right)_d} + \sum_{i=1}^k R(\Phi, \Theta) \rightarrow \min_{{\Phi}, {\Theta}} \\
\begin{aligned}
{\Phi} &= (\phi_{wt})_{W \times T}; & \phi_{wt} &\geq 0; & \sum_{w=1}^W \phi_{wt} &= 1; \\
{\Theta} &= (\theta_{td})_{T \times D};  &\theta_{td} &\geq 0; & \sum_{t=1}^T \theta_{td} &= 1.
\end{aligned}
\end{gather*}


Можно показать, что такая задача оптимизации соответствует максимизации совместного правдоподобия: 

\begin{gather*}
p(N |\Phi, \Theta) p(\Phi, \Theta | \vec\tau) \rightarrow \max_{\Phi, \Theta} \\
p(N |\Phi, \Theta)  = \prod_{w} \prod_d (\sum_t \phi_{wt} \theta_{td})^{n_{dw}}  \\
p(\Phi, \Theta | \vec\tau) = \prod_i \exp(\tau_i R_i(\Phi, \Theta))
\end{gather*}

Таким образом теперь:
\begin{itemize}
	\item $\vec{x}$ соответствует матрице частот $N$.
	\item $\vec{t}$ соответствует матрицам $\Phi$ и $\Theta$.
	\item $\vec\theta$ соответствует вектору коэффициентов регуляризации $\tau_i$
	\item $\vec\lambda$ соответствует матрицам $\vec\mu^\phi$ $\vec\mu^\theta$, $\vec\sigma^\phi$, $\vec\sigma^\theta$. Матрицы $\vec\mu^\phi$ $\vec\mu^\theta$, $\vec\sigma^\phi$, $\vec\sigma^\theta$ имеют размеры такие же, как соответствующие им $\Phi$ или $\Theta$. (Смысл будет пояснён в дальнейшем)
	\footnote{
		Для строгости последний столбец матриц $\vec\sigma$ положим тождественно равным единице.
	}
\end{itemize}

Зададим теперь распределение $q(\vec{t}| \vec\lambda) \equiv q(\Phi, \Theta | \vec\mu^\phi, \vec\mu^\theta, \vec\sigma^\phi, \vec\sigma^\theta)$ следующим образом. Рассмотрим две случайные матрицы $\vec\eps^\phi$, $\vec\eps^\theta$, такие, что каждый столбец $\vec\eps^\phi_t$ и $\vec\eps^\theta_d$ распределён по стандартному нормальному закону ($\mathcal{N}(0, I_w)$и $\mathcal{N}(0, I_T)$ сответственно). 

Тогда каждый столбец $\vec\phi_t$ матрицы $\Phi$ задаётся по следующей формуле: 
\begin{equation}
\vec\phi_t = \softmax(\vec\eps^\phi_t \odot  \vec\sigma^\phi_t + \vec\mu^\phi_t),
\label{eq:softmax_phi}
\end{equation}

где символом $\odot$ обозначается поэлементное произведение. Аналогично для $\Theta$: 
\begin{equation}
\vec\theta_d = \softmax(\vec\eps^\theta_d \odot  \vec\sigma^\theta_d + \vec\mu^\theta_d).
\label{eq:softmax_theta}
\end{equation}


Другими словами, столбцы матриц $\Phi$ и $\Theta$ получены независимо из Logit-Normal распределения с параметрами, заданными матрицами $\vec\mu^\phi, \vec\mu^\theta, \vec\sigma^\phi, \vec\sigma^\theta$.



Выпишем теперь логарифмы соответствующих плотностей:
\begin{equation}
\begin{aligned}
	\log p(\vec{x} | \vec{t}) &= \sum_d \sum_w n_{dw} \log (\sum_t \phi_{wt} \theta_{td}) \\
	\log \hat{p}(\vec{t} | \vec\theta)  &= \sum_i \tau_i R(\Phi, \Theta) \\
	\log q(\vec{t} | \vec\lambda) &=  \sum_t  \sum_w (\log \phi_{wt} + \log \sigma^\phi_{wt}) + \sum_d \sum_t (\log\theta_{td} + \log\sigma^\theta_{td}) + \mathrm{const}
\end{aligned}
\label{eq:density}
\end{equation}

\section{Совсем конкретно}

Рассмотрим поближе формулы \ref{eq:gradients_abstr}: и выпишем градиенты:

\begin{equation*}
\begin{aligned}
\frac
{\partial \mathcal{L}}
{\partial \vec\lambda}
&=
\underbrace{
	\mean_{q_0} 
	\nabla_{\vec\lambda}
	\log 
	p(\vec{x} | g(\vec\eps, \vec\lambda))
}_{(1)}
-
\underbrace{
	\mean_{q_0} 
	\nabla_{\vec\lambda}
	\log 
	q(g(\vec\eps, \vec\lambda) | \vec\lambda)
}_{(2)}
+
\underbrace{
	\mean_{q_0} 
	\nabla_{\vec\lambda}
	\log 
	\hat{p}(g(\vec\eps, \vec\lambda) | \vec\theta)
}_{(3)}
\\
\frac
{\partial \mathcal{L}}
{\partial \vec\theta}
&=
\underbrace{
	\mean_{q_0} 
	\nabla_{\vec\theta}
	\log 
	\hat{p}(g(\vec\eps, \vec\lambda) | \vec\theta)
}_{(4)}
-
\underbrace{
	\mean_{q_0}
		\nabla_{\vec\theta}
			\log
				Z(\vec\theta)
}_5
\end{aligned}
\end{equation*}

\subsection*{(1)}
\label{sec:(1)}

Воспользуемся \ref{eq:density}:
\begin{align*}
\nabla_{\vec\lambda}
	\log 
		p(\vec{x} | g(\vec\eps, \vec\lambda))
=
\nabla_{\vec\lambda}
	\sum_d
		\sum_w
			n_{dw}  \log (\sum_t \phi_{wt} \theta_{td})
= \\=
\sum_d
	\sum_w
		n_{dw}  
			\frac{1}{\sum_t \phi_{wt} \theta_{td}}
		\sum_t 
			\nabla_{\vec\lambda}
				\phi_{wt}(\vec\eps, \vec\lambda)
				\theta_{td}(\vec\eps, \vec\lambda)
= \\ 
\left[f_{wd} = \sum_t \phi_{wt} \theta_{td} = (\Phi \Theta)_{wd} \right]
\\ =
\sum_d
	\sum_w
		\frac{n_{dw}}{f_{dw}}
			\sum_t
				\nabla_{\vec{\lambda}}
					\phi_{wt}(\vec\eps, \vec\lambda)
					\theta_{td}(\vec\eps, \vec\lambda)
\end{align*}

Учитывая, что $\vec\phi_t$ не зависит от $\vec\phi_s$ для $t \neq s$, получим:

\begin{align*}
\frac
	{\partial \log p(\vec{x} | g(\vec\eps, \vec\lambda))}
	{\partial \mu_{vt}^\phi}
&=
	\sum_d
		\sum_w
			\frac{n_{dw}}{f_{dw}}
				\theta_{td}
				\frac{\partial \phi_{wt}}{\partial \mu_{vt}^\phi}
= \\ &=
	\sum_d
		\sum_w
			\frac{n_{dw}}{f_{dw}}
				\theta_{td}
				\phi_{wt} (\delta_{wv} - \phi_{vt})
= \\ &=
	\phi_{vt}
	\left(
		\sum_d
			\frac{n_{dv}}{f_{dv}} \theta_{td} 
		-
		\sum_d
			\sum_w
				\frac{n_{dw}}{f_{dw}} 
				\theta_{td}
				\phi_{wt}
	\right)
\end{align*}

Или, если mini-batch $\vec{x}$ состоит из одного документа $x_d$:
\begin{align*}
\frac
	{\partial \log p(x_d | g(\vec\eps, \vec\lambda))}
	{\partial \mu_{vt}^\phi}
=
	\phi_{vt}
	\left(
		\frac{n_{dv}}{f_{dv}} \theta_{td} 
		-
		\sum_w
		\frac{n_{dw}}{f_{dw}} 
		\theta_{td}
	\phi_{wt}
	\right)
\end{align*}

По аналогии нетрудно показать, что:

\begin{align*}
\frac
	{\partial \log p(x_d | g(\vec\eps, \vec\lambda))}
	{\partial \sigma_{vt}^\phi}
=
\vec\eps^{\phi}_{vt}
\phi_{vt}
\left(
\frac{n_{dv}}{f_{dv}} \theta_{td} 
-
\sum_w
\frac{n_{dw}}{f_{dw}} 
\theta_{td}
\phi_{wt}
\right)
\end{align*}

Для $\Theta$:
\begin{align*}
\frac
	{\partial \log p(\vec{x} | g(\vec\eps, \vec\lambda))}
	{\partial \mu_{sd}^\theta}
&=
\sum_w
	\sum_t
		\frac{n_{dw}}{f_{dw}}
			\phi_{wt}
			\frac
				{\partial \theta_{ts}}
				{\partial \mu_{sd}^\theta}
= \\ &=
\sum_w
	\sum_t
		\frac{n_{dw}}{f_{dw}}
			\phi_{wt}
			\theta_{td}	 
			(\delta_{st} - \theta_{sd})	
= \\ &=
\theta_{sd}
	\left(
	\sum_w
			\frac{n_{dw}}{f_{dw}}
				\phi_{ws}
	-
	\sum_w
		\frac
			{n_{dw}}
			{\cancel{f_{dw}}}
	\cancel{
		\sum_t
			\phi_{wt}
			\theta_{td}	 
	}
	\right)
= \\ &= 
\theta_{sd}
	\left(
	\sum_w
			\frac{n_{dw}}{f_{dw}}
				\phi_{ws}
	-
	n_d
	\right)
= \\ &=
\sum_w
	n_{dw}
	\frac
		{\phi_{ws} \theta_{sd}}
		{f_{dw}}
-
n_d \theta_{sd}
\end{align*}

\begin{align*}
\frac
	{\partial \log p(\vec{x} | g(\vec\eps, \vec\lambda))}
	{\partial \sigma_{sd}^\theta}
&=
\eps_{sd}^{\theta}
\theta_{sd}
	\left(
	\sum_w
		\frac{n_{dw}}{f_{dw}}
			\phi_{ws}
	-
	n_d
	\right)
\end{align*}

\subsection*{(2)}
\label{sec:(2)}

Зафиксируем тему $t$ и слово $v$:
\begin{equation}
\begin{aligned}
\frac
	{\partial \log q(g(\vec\eps, \vec\lambda)| \vec\lambda)}
	{\partial \mu_{vt}^\phi}
&=
\sum_w
\frac{\partial}{\partial \mu_{vt}}
\left(
\log \phi_{wt} + \log \sigma_{wt}
\right)
=
\sum_w 
	\frac1{\phi_{wt}} 
	\frac
		{\partial \phi_{wt}}
		{\partial \mu_{vt}}
=
\sum_w 
	\frac1{\cancel{\phi_{wt}}} 
	\cancel{\phi_{wt}}
	(\delta_{wv} - \phi_{vt})
= \\ &= 
\sum_w
	(\delta_{wv} - \phi_{vt})
=
1 - |W| \phi_{vt} 
\\
\frac
	{\partial \log q(g(\vec\eps, \vec\lambda)| \vec\lambda)}
	{\partial \sigma_{vt}^\phi}
&=
\sum_w 
	\left(
		\frac1{\phi_{wt}} 
		\frac
					{\partial \phi_{wt}}
					{\partial \sigma_{vt}}
		+
		 \frac{1}{\sigma_{wt}}
	\right)
= 
\sum_w 
	\left(
		\frac1{\cancel{\phi_{wt}}} 
		\eps_{vt}^\phi
		\cancel{\phi_{wt}}
		(\delta_{wv} - \phi_{vt})
		+
		 \frac{1}{\sigma_{wt}}
	\right)
= \\ &=
\eps_{vt}^\phi
-
|W| 
\eps_{vt}^\phi
\phi_{vt}
+
\sum_w
\frac{1}{\sigma_{wt}}
\end{aligned}
\label{eq:deriv_q_phi}
\end{equation}

Зафиксируем документ $d$ и тему $s$: 
\begin{equation}
\begin{aligned}
\frac
	 {\partial \log q(g(\vec\eps, \vec\lambda)| \vec\lambda)}
	 {\partial \mu_{sd}^\theta}
&=
\sum_t
	\frac
		{\partial}
		{\partial \mu_{sd}}
	\left(
		\log \theta_{td} + \log \sigma_{td}
	\right)
=
\sum_t
	\frac1{\theta_{td}}
	\frac
		{\partial \theta_{td}}
		{\partial \mu_{sd}}
=
\sum_t
	\frac1{\cancel{\theta_{td}}}
	\cancel{\theta_{td}}
	\left(
		\delta_{st} - \theta_{td}
	\right)
= \\ &=
\sum_t
	(\delta_	{ts} - \theta_{sd})
=
1 - |T| \theta_{sd} 
\\
\frac
	{\partial \log q(g(\vec\eps, \vec\lambda)| \vec\lambda)}
	{\partial \sigma_{sd}^\theta}
&=
\sum_t
	\left(
		\frac1{\theta_{td}}
		\frac
			{\partial \theta_{td}}
			{\partial \sigma_{sd}}
		+
		\frac1{\sigma_{td}}
	\right)
=
\sum_t
	\left(
		\frac1{\cancel{\theta_{td}}}
		\cancel{\theta_{td}}
		\eps_{sd}
		(\delta_{ts} - \theta_{sd})
		+
		\frac1{\sigma_{td}}
	\right)
= \\ &=
\eps_{sd} - \eps_{sd} \theta_{sd} |T| + \sum_t \frac1{\sigma_{td}}
\end{aligned}
\label{eq:deriv_q_theta}
\end{equation}

Стоит заметить, что во всех обновлениях связанных с данными $n_{dw}$ любое суммирование по словарю (который имеет большой размер), кроме $\sum_{w} \frac1{\sigma_{wt}}$ производится с весами $w_t$ производится с весами $n_{dw}$, что значит, что большую часть слов для каждого документа рассматривать не придётся и шаг градиентного спуска по данному документу будет асимптотически занимать столько же итераций, сколько слов в документе, а последнюю сумму можно преподсчитать и динамически обновлять по шагам градиентного спуска.

Таким образом производить проход по всему словарю для каждого документа не придётся. Асимптотика градиентного шага по одному батчу документов $D$, содержащем $N$ слов примерно $\BigO{N + |T|(|D| + |W|)}$.

\subsection*{(3)}
\label{sec:(3)}

\begin{align*}
\mean_{q_0} 
	\nabla_{\vec\lambda}
		\log 
			\hat{p}(g(\vec\eps, \vec\lambda) | \vec\theta)
=
\mean_{q_0} 
	\nabla_{\vec\lambda}
		\vec\theta^\top \vec{R}(g(\vec\eps, \vec\lambda))
=
\mean_{q_0} 
	\nabla_{\vec\lambda}
		\vec\theta^\top \vec{R}(g(\vec\eps, \vec\lambda))
= \\ =
\sum_i
	\theta_i
	\nabla_{\vec\lambda}
		R_i(g(\vec\eps, \vec\lambda))
\end{align*}

Конкретный вид формулы зависит от конкретного вида регялиризаторов. Заметим, что все регуляризаторы $R_i(\Phi, \Theta)$ должны быть дифференцируемы по матрицам $\Phi$ и $\Theta$.
 

\subsubsection*{Разреживание/сглаживание + частичное обучение}

Регуляризаторы сглаживания, разреживания и частичного обучения в общем виде задаются по следующей общей формуле:

\begin{equation*}
R(\Phi, \Theta) =  \sum_t \sum_w \beta_{wt} \log \phi_{wt} + \sum_d \sum_t  \alpha_{td} \log \theta_{td},
\end{equation*}

где знак соответствующий $\beta_{wt}$ и $\alpha_{td}$ отвечает тому разреживаем или сглаживаем тему $t$. Классический подход состоит в том, чтобы выделить множество $B$ сглаженных общеупотребительных топиков и множество $S$ специализированных тематических топиков. $T = B \sqcup S$

 \begin{align*}
\beta_{wt} = 
\begin{cases}
\beta_0,  & t \in B \\
-\beta_1, & t \in S\\
\end{cases}\\
\alpha_{td} =
\begin{cases}
 \alpha_0,  & t \in B \\
-\alpha_1, & t \in S
\end{cases}
\end{align*}
где $\alpha_0, \alpha_1, \beta_0, \beta_1$ --- неотрицательны.

Такой регуляризатор по факту представляет из себя сумму четырёх регуляризаторов, а коэффициенты $\alpha_0, \alpha_0, \beta_0$ и $\beta_1$ соответствуют настраиваемым коэффициентам $\vec\theta$

Производная по параметрам $\vec\lambda$ будет вычисляться абсолютно аналогично формулам \ref{eq:deriv_q_phi} \ref{eq:deriv_q_theta} за исключением того, что суммирование будет производится не по всем темам, а по соответствующему подмножеству ($B$ или $S$).

Для частичного обучения задаётся фиксированная структура распределения $\phi^0_{wt}$ и $\theta^0_{td}$ (априорные знания о принадлежности слов темам и тем документам) и используется следующая формула:

 \begin{align*}
 \beta_{wt} = 
 \begin{cases}
 \beta_0 + \tau_1 \phi^0_{wt},  & t \in B \\
 -\beta_1 + \tau_1 \phi^0_{wt}, & t \in S\\
 \end{cases}\\
 \alpha_{td} =
 \begin{cases}
 \alpha_0 + \tau_1 \theta^0_{td},  & t \in B \\
 -\alpha_1 + \tau_1 \theta^0_{td}, & t \in S
 \end{cases}
 \end{align*}

Теперь мы имеем 6 регуляризаторов и 6 настраиваемых коэффициентов.

\subsubsection*{Декореллирующий регуляризатор для тем} 

Для регуляризатора декорелляции функция $R(\Phi, \Theta)$ имееет следующий вид:

\begin{equation}
R(\Phi, \Theta) = 	-\frac12 \sum_{t\in T} \sum_{s\in T \setminus t } \sum_{w} \phi_{wt} \phi_{ws}
\end{equation}

Дифференцируя получаем:
\begin{equation}
\begin{aligned}
\frac
	{\partial}
	{\partial \mu^\phi_{vt} } R(\Phi, \Theta) 	
=& 
-\phi_{vt}
\sum_{s \in T \setminus t} 
	\left(
	\phi_{vs} 
	-
	\sum_w \phi_{ws} \phi_{wt}
 \right) \\
 \frac
	 {\partial}
	 {\partial \sigma^\phi_{vt} } R(\Phi, \Theta) 	
 =&
 -\phi_{vt} \eps_{vt}
	 \sum_{s \in T \setminus t} 
	 \left(
	 \phi_{vs} 
	 -
	 \sum_w \phi_{ws} \phi_{wt}
	 \right), \\
\end{aligned}
\label{eq:deriv_decor}
\end{equation}

Что даёт асимптотику шага $\BigO{N + |T||D| + |T|^2 |W|}$, если эффективно предподсчитать ковариации топиков перед подсчётом градиента.

\subsubsection*{Разреживающий регуляризатор тем}
Вместо разреживающего регуляризатора для выбора числа тем предполагается использовать перебор по числу тем $T$ с максимиацией обученного ELBO.

\subsection*{(4)}

\begin{align*}
\nabla_\theta
	\log
	\hat{p}(\vec{t} | \vec\theta)
=
\nabla_\theta
	\vec{\theta}^\top
	\vec{R}(\vec{t})
=
\vec{R}(\vec{t})
\end{align*}
Следовательно,

\begin{align*}
\mean_{q_0}
	\nabla_\theta
		\log
		\hat{p}(g(\vec\eps, \vec\lambda) | \vec\theta)
=
\mean_{q_0}
	\vec{R}(g(\vec\eps, \vec\lambda))
\end{align*}
\subsection*{(5)}
$Z(\vec\theta)$ --- нормировочная константа распределения $p(\vec{t} | \vec{\theta})$.

Т.к. $p(\vec{t} | \vec{\theta})$ --- принадлежит экспоненциальному семейству, где $\vec\theta$ --- естественные параметры, а $\vec{R}(\vec{t})$ --- достаточные статистики.
\begin{align*}
\mean_{q_0}
	\nabla_{\vec\theta}
	\log
		Z(\vec\theta)
=
\mean_{q_0}
\mean_{p(\vec{t} | \vec{\theta})} 
	\vec{R} (\vec{t})
= 
\mean_{p(\vec{t} | \vec{\theta})} 
\vec{R} (\vec{t})
\end{align*}

Также  можно использовать альтернативную формулу:
\begin{equation}
\begin{aligned}
\mean_{p(\vec{t} | \vec{\theta})}  
	\vec{R} (\vec{t}) 
=  
\mean_{q_0}
	\frac
		{\vec{R}({\vec{t}}) p(\vec{t} | \vec\theta)}
		{q_0(\vec{t})}
=
\frac1{Z(\vec\theta)}
\mean_{q_0}
	\frac
		{\vec{R}({\vec{t}}) \hat{p}(\vec{t} | \vec\theta)}
		{q_0(\vec{t})}
\\
Z(\vec\theta) 
= 
\int 
	\hat{p}(\vec{t} | \vec\theta) d\vec{t}  
= 
\int 
	\hat{p}(\vec{t} | \vec\theta) 
	\frac{q_0(\vec{t})}{q_0(\vec{t})} d\vec{t}  
=
\mean_{q_0}
\frac{\hat{p}(\vec{t} | \vec\theta)}{q_0(\vec{t})} 
\end{aligned}
\label{eq:importance} 
\end{equation}

Для оценки $\mean_{p(\vec{t} | \vec{\theta})}  \vec{R} (\vec{t})$ предлагается использовать одну из следующих схем:
\begin{enumerate}
	\item Во время семплинга из $q_0(\vec\eps)$ делать несколько итераций MCMC по $\hat{p}(\vec{t} | \vec\theta)$ (Contrastive Divergence подход)
	\item Во время семплинга из $q_0(\vec\eps)$ пользуясь преобразованием \ref{eq:importance} использовать те же семплы $\vec{t} \sim q_0$ для подсчёта $\mean_{q_0}\frac {\vec{R}({\vec{t}}) p(\vec{t} | \vec\theta)}{q_0(\vec{t})}$, а также $\mean_{q_0}\frac{\hat{p}(\vec{t} | \vec\theta)}{q_0(\vec{t})}$. Для вычисления градиента использовать текущее значение аппроксимации отношения двух матожиданий. (Importance sampling подход)
\end{enumerate}

\begin{algorithm}
	\caption{Алгоритм обучения (не онлайновая версия)}
	\label{alg1}
	\begin{algorithmic}
		\REQUIRE $\vec{F} \in \R^{W \times D}$ --- term-frequency matric
		\ENSURE Vector of regulatization coeffitients $\vec\tau$; Matricies $\Phi$ and$\Theta$
		\STATE Initialize $\vec\mu^\phi, \vec\mu^\theta, \vec\sigma^\phi, \vec\sigma^\theta$
		\WHILE{not converged}
		\STATE Sample matricies $\vec\eps^\phi \in \R^{W \times T} \vec\eps^\theta \in \R^{T \times D}$:
		\STATE $\eps_{i, j} \leftarrow \mathcal{N}(0, 1)$ i.i.d componentwise 
		\STATE $\vec\phi_t := \softmax(\vec\eps^\phi_t \odot  \vec\sigma^\phi_t + \vec\mu^\phi_t), t=1, \dots, T$
		\STATE $\vec\theta_d = \softmax(\vec\eps^\theta_d \odot  \vec\sigma^\theta_d + \vec\mu^\theta_d), d=1, \dots, D$
		\STATE Compute gradients of $\mathcal{L}$ over $\vec\mu^\phi, \vec\mu^\theta, \vec\sigma^\phi, \vec\sigma^\theta$: accroding to formulae in sections (1), (2) and (3)
		\STATE 
		\STATE $\dots$
		\ENDWHILE
	\end{algorithmic}
\end{algorithm}

В онлайновой версии алгоритма цикл алгоритма прогоняется несколько раз по очередному батчу данных. При этом значения $\vec\tau$, $\vec\mu^\phi$ и $\vec\sigma^\phi$ сохраняются с предыдущей итерации (с обработки предыдущего батча). Параметры $\vec\mu^\theta$ и $\vec\sigma^\theta$ инициализируются снова для нового батча.

Обратим внимание, что после обработки очередного батча необходимо сделать точечную оценку тематических профилей документов $\vec\theta_d$ (Мы закончили их обрабатывать, надо для них что-то вернуть). Зная оценку распределения на $\vec\theta_d \sim \mathrm{LogitNormal}(\vec\mu^\theta_d, \vec\sigma^\theta_d)$. Можно попробовать один из нескольких подходов:

\begin{enumerate}
	\item Взять медиану распределения: $\hat{\vec\theta_d} \approx \softmax(\mu^\theta_d)$ (Быстро, но не факт, что хорошо и правильно)
	\item Приближённо считать моду распределения $\hat{\vec\theta_d}  \approx \argmax_{\vec\theta} \mathrm{LogitNormal}(\vec\theta_d | \vec\mu^\theta_d, \vec\sigma^\theta_d)$. (Долго, но точно хорошо)
	\item Семплировать из распределения $\hat{\vec\theta_d} \sim \mathrm{LogitNormal}(\vec\theta_d | \vec\mu^\theta_d, \vec\sigma^\theta_d)$. (Быстро, но неточно. Может быть эффективным если $\vec\sigma^\theta_d$ --- маленькое и распределение узкое)
\end{enumerate}

\section{Возможные проблемы и перспективы развития}


%\renewcommand{\refname}{Список литературы}
%\addcontentsline{toc}{section*}{\refname}

%\bibliography{KhalmanBiblio}{}
%\bibliographystyle{plain}

\end{document}